package com.mh.jishi.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mh.jishi.entity.K31;
import com.mh.jishi.mapper.K31Mapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author Lizr
 * @Description 用户服务层
 * @CreateTime 2021-12-27 下午 3:56
 **/
@Service
@DS("slave_1")
public class K31Service extends ServiceImpl<K31Mapper, K31> {
    @Resource
    public K31Mapper k31Mapper;
    public List<K31> getCardTimes(String userNo,String yearMonth) {
        if (yearMonth.isEmpty()) {
            return k31Mapper.getCardTimes(userNo);
        }else if(yearMonth.equals("8")){
            return k31Mapper.getOverTimes(userNo);
        }else {
            return k31Mapper.getCardTimesBy(userNo,yearMonth);
        }

    }
}
