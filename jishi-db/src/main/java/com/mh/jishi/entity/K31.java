package com.mh.jishi.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@TableName(value = "k31")
public class K31 {
    //加班时长(计算得来)
    private String diffhour;

    // 上班卡
    private String K3104;
    // 下班卡
    private String K3106;
    // 星期
    private String K31083;


}
