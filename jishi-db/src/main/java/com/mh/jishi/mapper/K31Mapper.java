package com.mh.jishi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mh.jishi.entity.K31;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Author Lizr
 * @Description 用户mapper
 * @CreateTime 2021-12-27 下午 3:50
 **/
@Mapper
public interface K31Mapper extends BaseMapper<K31> {
    @Select({"SELECT top 3   k.K31083,CONVERT(VARCHAR(100),k.K3104,20) K3104 , CONVERT(VARCHAR(100), k.K3106,20) K3106 \n" +
            "FROM  k31 k  LEFT JOIN A01 a ON k.A0188 = a.A0188  LEFT JOIN K_Result re ON re.BM0000 = k.RType  \n" +
            "WHERE  a.A0190 = #{userNo}    \n" +
            "AND CONVERT ( NVARCHAR ( 6 ), k.D9999, 112 ) = '202206' \n" +
            "AND k.D9999<= CONVERT ( NVARCHAR ( 10 ), getdate( ), 121 )  \n" +
            "ORDER BY  k.D9999  desc \n "})
    List<K31> getCardTimes(@Param("userNo") String userNo);
    @Select({"SELECT  k.K31083,CONVERT(VARCHAR(100),k.K3104,20) K3104 , CONVERT(VARCHAR(100), k.K3106,20) K3106 \n" +
            "FROM  k31 k  LEFT JOIN A01 a ON k.A0188 = a.A0188  LEFT JOIN K_Result re ON re.BM0000 = k.RType  \n" +
            "WHERE  a.A0190 = #{userNo} \n" +
            "AND CONVERT ( NVARCHAR ( 6 ), k.D9999, 112 ) = #{yearMonth} \n" +
            "AND k.D9999<= CONVERT ( NVARCHAR ( 10 ), getdate( ), 121 )  \n" +
            "ORDER BY  k.D9999  desc \n "})
    List<K31> getCardTimesBy(@Param("userNo") String userNo,@Param("yearMonth") String yearMonth);

    @Select({"SELECT case when  diffminute%60>=30  then diffminute/60+0.5\n" +
            "else diffminute/60 end as diffhour\n" +
            ",K31083,K3106 FROM(\n" +
            "SELECT  datediff(MINUTE,'19:30:00',CONVERT(NVARCHAR(10),k.K3106,24))  as diffminute\n" +
            ",k.K31083,CONVERT(VARCHAR(100),k.K3104,20) K3104 , CONVERT(VARCHAR(100), k.K3106,20) K3106, k.D9999 \n" +
            "FROM  k31 k  LEFT JOIN A01 a ON k.A0188 = a.A0188  LEFT JOIN K_Result re ON re.BM0000 = k.RType  \n" +
            "WHERE  a.A0190 = #{userNo}  \n" +
            "AND k.D9999<= CONVERT ( NVARCHAR ( 10 ), getdate( ), 121 ) \n" +
            "AND  k.D9999>= DATEADD(MONTH,-2,DATEADD(mm, DATEDIFF(mm,0,GETDATE()),0))\n" +
            "AND CONVERT(NVARCHAR(10),k.K3106,24)>'19:00:00'\n" +
            ")kk ORDER BY D9999 DESC "})
    List<K31> getOverTimes(@Param("userNo") String userNo);

}
