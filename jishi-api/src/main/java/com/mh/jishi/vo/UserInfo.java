package com.mh.jishi.vo;

import lombok.Data;

/**
 * @Author Lizr
 * @Description 登录inf
 * @CreateTime 2021-12-28 下午 12:00
 **/
@Data
public class UserInfo {
    private int id;
    private int status;
    private String userNo;
    private String nickName;
    private String avatarUrl;
    private String signDesc;
    private String weixinOpenid;


}
