package com.mh.jishi.ucontroller;

import com.mh.jishi.annotation.ApiDesc;
import com.mh.jishi.entity.K31;
import com.mh.jishi.entity.TStorage;
import com.mh.jishi.entity.TUser;
import com.mh.jishi.service.K31Service;
import com.mh.jishi.service.TUserService;
import com.mh.jishi.storage.StorageService;
import com.mh.jishi.util.HttpUtil;
import com.mh.jishi.util.JacksonUtil;
import com.mh.jishi.util.ResponseUtil;
import com.mh.jishi.util.WxResponseCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping(value = "/api/wx")
@SuppressWarnings("all")
@Api(tags = "用户信息管理")
public class AppWxController {
    @Autowired
    private TUserService userService;
    @Resource
    private StorageService service;
    @Resource
    private K31Service k31Service;


    @ApiOperation(value = "登录凭证校验", notes = "通过 wx.login 接口获得临时登录凭证 code 后传到开发者服务器调用此接口完成登录流程。更多使用方法详见小程序登录。")
    @ApiDesc(methodDesc = "登录凭证校验。通过 wx.login 接口获得临时登录凭证 code 后传到开发者服务器调用此接口完成登录流程。更多使用方法详见小程序登录。")
    @GetMapping("/sns/jscode2session")
    public ResponseUtil jscode2session(@RequestParam(value = "code") String code){
        String url="https://api.weixin.qq.com/sns/jscode2session";
        Map<String, String> params = new HashMap<>();
        params.put("appid", WxResponseCode.APPID);
        params.put("secret",WxResponseCode.SECRET);
        params.put("js_code",code);
        params.put("grant_type","authorization_code");
        String res=HttpUtil.sendPost(url,params);

        return ResponseUtil.ok("获取成功",  JacksonUtil.toMap(res));
    }
    @ApiDesc(methodDesc = "文件上传")
    @PostMapping("/uploadLocalFiles")
    public Object uploadLocalFiles(MultipartFile file) throws IOException {
//        logger.info(">>>"+files.toString());
        Assert.notNull(file, "文件列表不能为空");
        LinkedList<TStorage> tStorages = new LinkedList<>();
        if (!file.isEmpty()) {
            InputStream stream = file.getInputStream();
            long size = file.getSize();

            String contentType = file.getContentType();
            String fileName = file.getOriginalFilename();
            TStorage save = service.store(stream, size, contentType, fileName);
            tStorages.add(save);
        }

        return ResponseUtil.ok("获取成功",tStorages);
    }
    @ApiOperation(value = "更新用户信息", notes = "更新昵称")
    @ApiDesc(methodDesc = "根据type类型更新用户信息：1-头像，2-员工编号，3-昵称，4-个签")
    @PostMapping("/updateUser")
    public ResponseUtil updateUser(@RequestParam(value = "openid") String openid,
                                   @RequestParam(value = "updateData") String updateData,
                                   @RequestParam(value = "type") int type
                                   ){

        TUser tUser = userService.queryByOpenId(openid);
        if (ObjectUtils.isEmpty(tUser)) {
            return ResponseUtil.fail(WxResponseCode.AUTH_INVALID_ACCOUNT,"查无账号。");
        }
        switch (type){
            case WxResponseCode.AVATAR :
                tUser.setAvatar(updateData);
                break;
            case WxResponseCode.USER_NO:
                tUser.setUserno(updateData);
                break;
            case WxResponseCode.NICK_NAME :
                tUser.setNickname(updateData);
                break;
            case WxResponseCode.SIGN_DESC :
                tUser.setSigndesc(updateData);
                break;
            default:
                return ResponseUtil.fail("查无此类型。");
        }

        final boolean updateRes = userService.updateById(tUser);
        if(updateRes){
            return  ResponseUtil.ok();
        }else {
            return  ResponseUtil.fail();
        }

    }


    @ApiOperation(value = "获取打卡时间", notes = "获取前三日的上下班打卡时间和星期。")
    @ApiDesc(methodDesc = "根据人员编号获取打卡时间")
    @GetMapping("/getCardTimes")
    public ResponseUtil getCardTimes(@RequestParam(value = "userno") String userno,
                                     @RequestParam(value = "yearMonth", required = false,defaultValue = "") String yearMonth
                                     ){
    //胡弄官方的历史数据
        if(StringUtils.isNotEmpty(userno))
            userno="2206148";
        if(StringUtils.isNotEmpty(yearMonth)){
            if(!yearMonth.equals("8"))
                yearMonth="202206";
        }

        List<K31> k31List= k31Service.getCardTimes(userno,yearMonth);
        return ResponseUtil.ok("获取成功",  k31List);
    }

    @ApiOperation(value = "获取人员列表", notes = "获取所有注册人员。")
    @ApiDesc(methodDesc = "根据人员状态控制权限")
    @PostMapping("/getSetUsers")
    public ResponseUtil getSetUsers(@RequestParam(value = "userids",required = false) List<Integer> userids,
                                    @RequestParam(value = "userstatus",required = false,defaultValue = "0") int userstatus
                                    ){

        if (CollectionUtils.isEmpty(userids)) {
            return ResponseUtil.ok("获取成功", userService.queryList());
        }else{
            List<TUser> tUsers = userService.listByIds(userids);
            for (TUser tUser : tUsers) {
                tUser.setStatus(userstatus);
            }
            if(userService.updateBatchById(tUsers)){
                return ResponseUtil.ok("更新成功");
            }else {
                return ResponseUtil.fail("更新失败");
            }
        }

    }
    @ApiOperation(value = "获取九宫格列表", notes = "获取九宫格数据。")
    @ApiDesc(methodDesc = "根据人员状态控制菜单列表权限")
    @GetMapping("/getMenuList")
    public ResponseUtil getMenuListByStatus(@RequestParam(value = "userstatus",required = false,defaultValue = "1") int userstatus){
        String baseJsonStr="[{\"id\":\"kaoqin\",\"name\":\"考勤\",\"url\":\"../../packageCloud/pages/kaoqin/check-work\",\"icon\":\"./resources/image/card.png\"},{\"id\":\"jiaban\",\"name\":\"加班\",\"url\":\"../../packageCloud/pages/jiaban/work-extra\",\"icon\":\"./resources/image/addhours.png\"},{\"id\":\"user\",\"name\":\"信息\",\"url\":\"../../packageCloud/pages/user/user-info\",\"icon\":\"./resources/image/empinfo.png\"}]";
        String adminjsonStr="[{\"id\":\"manageuser\",\"name\":\"管理\",\"url\":\"../../packageCloud/pages/manageuser/edit-user\",\"icon\":\"./resources/image/console.png\"},{\"id\":\"other\",\"name\":\"\",\"url\":\"index\",\"icon\":\"\"},{\"id\":\"other1\",\"name\":\"\",\"url\":\"index\",\"icon\":\"\"}]";
        JSONArray baseArray = JSONArray.fromObject(baseJsonStr);
        if(userstatus==99999){
            JSONArray adminArray = JSONArray.fromObject(adminjsonStr);
            baseArray.addAll(adminArray);
            return ResponseUtil.ok("获取成功",baseArray);
        }
        if(userstatus!=0){
            for (int i=0;i<baseArray.size();i++) {
                baseArray.getJSONObject(i).put("url","index");
            }
        }

        return ResponseUtil.ok("获取成功",baseArray);
    }


}
