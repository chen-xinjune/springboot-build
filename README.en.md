# 时光记录者

#### Description
光阴记录者，是一款适用于生活中信息记录和查询的小程序，比如：记账，日历，天气，备忘录，办公，信息查询，是记录生活提高效率的工具

#### Software Architecture
Software architecture description

#### Installation
| 依赖             | 版本                 |
|----------------|--------------------|
| java            | >= 1.8            |
| mysql       | >= 5.6               |
| redis       | >= 3.0             |


#### Instructions

* 后台管理shiro权限控制
* 移动端api jwt+token
* 腾讯云短信模版
* Redis整合
* logback日志
* queue延迟任务队列实现
* 移动端api完成程序常见接口、登录注册、用户个人信息修改、发送验证码、意见反馈、帮助信息
* 后台管理常见接口、管理员管理、意见反馈管理、帮助信息管理、用户管理
* 极光推送配置
* 对象存储配置
* 微信支付、支付宝支付、苹果支付配置
* 支付回调控制层
* Swagger 支持
* Druid 数据监控
* 待整合RabbitMQ。。。

#### Contribution

| 模块              | 介绍                 |
|-----------------|--------------------|
| doc             | 项目文件 目录            |
| doc.data        | 项目数据               |
| doc.desc        | 项目说明文件             |
| doc.sql         | sql                |
| jishi-admin     | 后台管理接口、服务          |
| jishi-api       | 移动端api             |
| jishi-core      | 核心公共组件             |
| jishi-db        | 数据库、服务层、实体类、mapper |
| jishi-start     | 程序启动相关             |
| logs            | 主程序日志目录            |
| files           | 本地对象存储存储文件夹        |
| jishi-vue-admin | vue管理后台            |

#### Gitee Feature

> Redis <br>
账号: ****<br>
密码: ****<br>
Url：<br>
